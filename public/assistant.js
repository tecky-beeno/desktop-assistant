let msgBox = document.querySelector('.msg-box')

let actions = {
  mouseover: [
    {
      selector: 'header',
      text: [
        "Don't touch my head (1)",
        "Don't touch my head (2)",
        "Don't touch my head (3)",
      ],
    },
    {
      selector: '#article-1',
      text: ["I know you'll read article 1"],
    },
  ],
  click: [
    {
      selector: 'header',
      text: ["Don't move"],
    },
  ],
}

let selectors = ['header', '#article-1', '#article-2', 'footer']

for (let type in actions) {
  for (let item of actions[type]) {
    let element = document.querySelector(item.selector)
    let count = 0
    element.addEventListener(type, event => {
      msgBox.textContent = item.text[count]
      count = (count + 1) % item.text.length
    })
  }
}
