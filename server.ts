import express from 'express'
import jsonfile from 'jsonfile'
import { print } from 'listening-on'

let app = express()

app.use(express.static('public'))

app.use(express.urlencoded({ extended: false }))

type Actions = Record<string, Array<{ selector: string; text: string[] }>>

let actionsPromise: Promise<Actions> = jsonfile
  .readFile('actions.json')
  .catch(() => ({}))

function saveActions(actions: Actions) {
  jsonfile.writeFile('actions.json', actions)
}

app.get('/actions', (req, res) => {
  actionsPromise.then(actions => {
    res.json(actions)
  })
})

app.post('/actions/type', (req, res) => {
  actionsPromise.then(actions => {
    if (!actions[req.body.type]) {
      actions[req.body.type] = []
      saveActions(actions)
    }
    res.json(actions)
  })
})

app.post('/actions/type/selector', (req, res) => {
  actionsPromise.then(actions => {
    let itemList = actions[req.body.type]
    let item = itemList.find(item => item.selector == req.body.selector)
    if (!item) {
      itemList.push({ selector: req.body.selector, text: [] })
      saveActions(actions)
    }
    res.json(actions)
  })
})

app.patch('/actions/type/selector', (req, res) => {
  actionsPromise.then(actions => {
    const itemList = actions[req.body.type]
    const item = itemList.find(item => item.selector == req.body.selector)
    const newItem = itemList.find(
      item => item.selector == req.body['new-selector'],
    )
    if (!item) {
      res.status(404).json({ message: 'Not Found' })
      return
    }
    if (newItem) {
      item.text.forEach(text => {
        if (!newItem.text.includes(text)) {
          newItem.text.push(text)
        }
      })
      let itemIndex = itemList.indexOf(item)
      itemList.splice(itemIndex, 1)
    } else {
      item.selector = req.body['new-selector']
    }
    saveActions(actions)
    res.json(actions)
  })
})

app.post('/actions/type/selector/text', (req, res) => {
  actionsPromise.then(actions => {
    let itemList = actions[req.body.type]
    let item = itemList.find(item => item.selector == req.body.selector)
    if (!item) {
      res.status(404).json({ message: 'Not Found' })
      return
    }
    if (!item.text.includes(req.body.text)) {
      item.text.push(req.body.text)
      saveActions(actions)
    }
    res.json(actions)
  })
})

let port = 3000
app.listen(port, () => {
  print(port)
})
